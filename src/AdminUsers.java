public class AdminUsers
{
	private String [] botOwners; //array to store list of bot owners
	private final int MAXOWNERS = 100; //max number of owners allowed

	/**
		Constructor method for AdminUsers
		This method ensures that each index of botOwners is
		set to ""
	*/
	public AdminUsers(){
		botOwners = new String[MAXOWNERS];
		for (int i = 0; i < MAXOWNERS; i++){
			botOwners[i] = ""; 
		}
	}

	/**
		Removes someone from the list of botOwners
		@param aUser user to be removed from botOwners[]
	*/
	public void removeOwner(String aUser){
		for (int i = 0; i < MAXOWNERS; i++){
			if (botOwners[i].equals(aUser)){
				//if the user is found, then replace with a '@' 
				//which symbolizes a index marked for overwritting later
				//on
				botOwners[i] = "@";
			}
		}
	}

	/**
		@return This method returns a string of all the BotOwners with a 
		space between each one.
	*/
	public String getOwners(){
		String res = "";
		for (int i = 0;i < MAXOWNERS; i++){
			if (!botOwners[i].equals("@") && !botOwners[i].equals("")){
				res = res + botOwners[i] + " ";
			}
			if (botOwners.equals(""))
				break;
		}

		return res;
	}

	/**
	This method is used to add a new owner to the current array
	of botOwners
	@param aOwner a string passed in and will added to botOwners[]
	*/
	public void addOwner(String aOwner){
		for (int i = 0; i < MAXOWNERS; i++){
			//finds the first '@' character or empty string 
			//to insert new botOwner
			if (botOwners[i].equals("@") || botOwners[i].equals("")){
				botOwners[i] = aOwner;
				break;
			}
		}
	}

	/**
		This method is used to determine if a User is also
		in the botOwner[]
		@param aUser passed in and used as a comparison. 
		@return Returns true if aUser is in array and false otherwise
	*/
	public Boolean isOwner(String aUser){
		for (int i = 0; i < MAXOWNERS ; i++){
			if (botOwners[i].equals(""))
				return false;
			if (botOwners[i].equals(aUser)){
				return true;
			}
		}
		return false;
	}
}