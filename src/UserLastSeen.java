/**
 * Created on 4/26/2015.
 * This class retrieves and stores data related to when a user was last seen in the channel.
 * This data is stored as a serialzed self balancing binary tree object for efficient and fast performance
 */

import java.util.*;
import java.io.*;
import java.util.Date;

public class UserLastSeen implements java.io.Serializable {
    private TreeMap tMap;

    /**
     Constructor method for UserLastSeen
     This method determines whether or not a serialized object of TreeMap has
     already been saved in the current directory. If a serialized object exist
     it is read into memory, Otherwise it is created
     */
    public UserLastSeen(){
        //check to see if serialized object already exist in the current directory
        File f = new File("seenObj.bin");
        if(f.exists() && !f.isDirectory()) {
            //file exist and now we must read from the file
            try{
                ObjectInputStream savedSer = null;
                savedSer = new ObjectInputStream(new BufferedInputStream(new FileInputStream("seenObj.bin")));
                tMap = (TreeMap) savedSer.readObject();
            }
            catch (ClassNotFoundException e){
                e.printStackTrace();
            }
            catch (IOException e ){
                e.printStackTrace();
            }
        }
        else{
            //file does not exist, so we must serialize and store the object
            try {
                tMap = new TreeMap();
                ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("seenObj.bin"));
                out.writeObject(tMap);
                out.close();
            }
            catch (IOException e ){
                e.printStackTrace();
            }
        }
    }

    /**
     This method stores the last time a user was seen into the binary tree
     @param aUser User that was seen
     */
    public void seenUser(String aUser){
        Date userSeenOn = new Date();

        tMap.put(aUser, userSeenOn); //save the last time this user was seen in the binary tree object
    }

    /**
     * @param aUser User to query about when they were last seen
     * @return res Message to be sent back as to whether not the User was found in the binary tree
     */
    public String when(String aUser){
        String res = "";
        Date currDateTime = new Date();

        if (tMap.containsKey(aUser)){
            //get the difference in the dates
            Date startDate = (Date)tMap.get(aUser);
            long different = currDateTime.getTime() - startDate.getTime();

            long secondsInMilli = 1000;
            long minutesInMilli = secondsInMilli * 60;
            long hoursInMilli = minutesInMilli * 60;
            long daysInMilli = hoursInMilli * 24;

            long elapsedDays = different / daysInMilli;
            different = different % daysInMilli;

            long elapsedHours = different / hoursInMilli;
            different = different % hoursInMilli;

            long elapsedMinutes = different / minutesInMilli;
            different = different % minutesInMilli;

            long elapsedSeconds = different / secondsInMilli;

            //prepare response to return to function caller
            res = "This user was last seen " + elapsedDays + " days, " + elapsedHours + " hours, " + elapsedMinutes + " minutes, and " + elapsedSeconds + " seconds ago.";
        }
        else{
            res = "I've never seen this user in the channel!";
        }

        return res;
    }

    /**
     * This method is used to save the TreeMap object in memory to the harddrive so that it may be persistantly used the
     * next time the bot is executed
     */
    public void saveMap(){
        try{
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("seenObj.bin"));
            out.writeObject(tMap);
            out.close();
        }
        catch (IOException e ){
            e.printStackTrace();
        }
    }
}
