import java.io.*;
import java.util.Date;

public class ChatLogging {
    private FileWriter writer;
    private String filename = "chat.log";
    ErrorLogging errorLogger = new ErrorLogging();
    private Date dateObj;

    public ChatLogging(){}

    public ChatLogging(String fname){
        filename = fname;
    }

    public void log(String user, String [] parsedLine){
        try {
            writer = new FileWriter(filename,true); //the true will append the new data
            dateObj = new Date();
            String res = dateObj.toString() + ": ";
            //check to see if the user is joining
            if (parsedLine[0].toCharArray()[0] == '#') {
                res = "(" + user + " has joined " + parsedLine[0] + ")";
            }
            else if (parsedLine[0].equals("Leaving")){
                res = "(" + user + " has left the channel";
            }
            else {
                res += user + ": ";
                for (int i = 0; i < parsedLine.length; i++) {
                    res += parsedLine[i] + " ";
                }
            }
            res += "\r\n";
            writer.append(res);
            writer.flush();
            writer.close();
        }
        catch (ArrayIndexOutOfBoundsException e){

        }
        catch(Exception e){
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errorLogger.log(errors.toString());
        }
    }
}
