import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

public class ErrorLogging {
    private FileWriter writer;
    private Date dateObj;
    private String filename = "Error.log";

    public ErrorLogging(){}

    public ErrorLogging(String fname){
        filename = fname;
    }

    public void log(String stacktrace){
        try {
            writer = new FileWriter(filename,true); //the true will append the new data

            dateObj = new Date();
            String res = "";
            res += dateObj.toString() + ": ";
            res += stacktrace;
            writer.append(res + "\r\n\r\n");
            writer.flush();
            writer.close();

            System.out.println("An error has been caught and successfully logged in Error.log!");
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}
