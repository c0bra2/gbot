import java.util.Random;

public class RandomName
{
	private String name;
	private String prefix;
	private boolean useRandomPrefix = false;

	public RandomName(){
		Random randomGen = new Random();
		StringBuilder sb = new StringBuilder();

        //add 5 random letter characters to the string builder
        for (int i = 0; i <= 4; i++) {
            sb.append(letterChar(randomGen));
        }

		//add 5 random digit characters to the string builder
		for (int i = 0; i <= 4; i++){
			sb.append(digitChar(randomGen));
		}

		//store the string builder result into name
		name = sb.toString();
	}

    /**

      @param aPrefix a string value to be used as the name prefix
     */
    public RandomName(String aPrefix){
        Random randomGen = new Random();
        StringBuilder sb = new StringBuilder();

        sb.append(aPrefix);
        prefix = aPrefix;

        //add 5 random digit characters to the string builder
        for (int i = 0; i <= 4; i++){
            sb.append(digitChar(randomGen));
        }

        //store the string builder result into name
        name = sb.toString();
    }

	/**
		@return accessor method for name
	*/
	public String getName(){
		return name;
	}

	/**
	 @return accessor method for prefix
	 */
	public String getPrefix(){
		return prefix;
	}

	/**
		@param aRandom an instance of Random class to pass into function
		@return Returns a random lowercase character
	*/
	private char letterChar (Random aRandom){
		//get the range, casting to long to avoid overflow problems
		long range = (long)(122.00 - (97.00 + 1));
		//compute a fraction of the range, 0 <= frac < range
		long fraction = (long)(range * aRandom.nextDouble());
		int randomNumber = (int)(fraction + 97.00);
		return (char)(randomNumber);
	}

	/**
		@param aRandom an instance of Random class to pass into function
		@return Returns a random digit character
	*/
	private char digitChar (Random aRandom){
		//get the range, casting to long to avoid overflow problems
		long range = (long)(57.00 - (48.00 + 1));
		//compute a fraction of the range, 0 <= frac < range
		long fraction = (long)(range * aRandom.nextDouble());
		int randomNumber = (int)(fraction + 48.00);
		return (char)randomNumber;
	}
}