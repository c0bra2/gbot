import java.io.*;
import java.net.*;
import java.util.Scanner;
/**
	@author Jacob Mason
	this class represents an irc bot framework.
*/
public class JHat
{
	private Socket sock;
	private BufferedWriter out;
	private BufferedReader in;
	private String channel;
	private String botNick;
	private String rawString;
    private String fullstring;
	private String [] whichChan;
	private String [] talker;
	private String [] line;
	private boolean connected = false;
	ErrorLogging errorLogger = new ErrorLogging();

	public JHat(String aNick, String aServer, String aChan, int aPort)
	{
		try
		{
			// setup connection and i/o streams
			System.out.println("Initializing...");
			sock = new Socket(aServer, aPort);
			out = new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));
			in = new BufferedReader(new InputStreamReader(sock.getInputStream()));

			// tell the server who the bot is
			if (sock.isConnected())
			{
				connected = true;
				System.out.println("[Connected Successfully]");
				send("NICK " + aNick + "\r\n");
				send("USER " + aNick + " " + aServer + " bla :" + aNick +
				"\r\n");
				joinChan(aChan);

				// store botNick
				botNick = aNick;
			}

		}
		catch(IOException e)
		{
			errorLogger.log(e.toString());
		}
	}

	/**
		Joins to a channel
		@param chan channel to join 
	*/
	public void joinChan(String chan)
	{
		channel = chan;
		send(" JOIN " + chan + "\r\n");
	}

    /**
     * @param in
     * @return Returns a string with the first token parsed from it
     */
    private String stripFirstToken(String in){
        boolean spaceEncountered = false;
        String res = "";

        for (int i = 0; i < in.length(); i++){
            if (spaceEncountered){
                res += in.toCharArray()[i];
            }
            if (in.toCharArray()[i] == ' '){
                spaceEncountered = true;
            }
        }
        return res;
    }

	/**
		parse method, parses data from the server
		should be called before accessor methods like getLine()
		parse() also takes care of any PING request
	*/ 
	public void parse()
	{
		rawString = recv();
		line = rawString.split(":");
		if (line.length >= 3){
            fullstring = stripFirstToken(line[2]);
			line = line[2].split("\\s");
        }
		
		// get channel of person talking
		whichChan = rawString.split(":");
		if (whichChan.length >= 2)
			whichChan = whichChan[1].split("\\s");

		// get nick of the person talking
		talker = rawString.split(":");
		if (talker.length >= 2)
			if (talker[1].contains("!"))
				talker = talker[1].split("!");

		// take care of PING request
		if (rawString.contains("PING"))
			pong();
	}

	/**
	 Counts the number of users currently in the channel
	 */
	public int countUsers(){
		put("NAMES " + channel);
		return (recv().split(":")[2].split(" ").length);
	}

	/**
		write stuff to irc chan
		@param data the stuff to write
	*/
	public void write(String data)
	{
		send("PRIVMSG " + channel + " :" + data + "\r\n");
	}

    /**
     write stuff to irc chan
     @param  aUser user to send to
     @param data the stuff to write
     */
    public void write(String aUser, String data)
    {
        send("NOTICE " + aUser + " :" + data + "\r\n");
    }

	/**
		send data to the server.
		similar to send(), but for
		use when you need to use
		MODE and stuff
		@param data the stuff to send
	*/
	public void put(String data)
	{
		send(data + "\r\n");
	}

	/**
		@return accessor method for connected
	*/
	public boolean getIsConnected()
	{
		return connected;
	}
	
	/**
		@return accessor method for talker
	*/
	public String getTalker()
	{
		return talker[0];
	}

	/**
		@return accessor method for initialized channel
	*/
	public String getChan()
	{
		return channel;
	}

	/**
	 * @return accessor method for botNick
	 */
	public String getBotNick(){
		return botNick;
	}
	/**
		@return accessor method for whichChan
	*/
	public String getWhichChan()
	{
		if (whichChan.length >= 3)
			return whichChan[2];
		return "";
	}

	/**
		@return accessor method for line
	*/
	public String[] getLine()
	{
		String[] arr = {""};
		if (line.length < 1){
			return arr;
		}
		else
			return line;
	}

	/**
		@return accessor method for channel and nick, returned as array
	*/
	public String[] getSelf()
	{
		String [] res = {botNick, channel};
		return res;
	}

	/**
		@return accessor method for rawString
	*/
	public String getRS()
	{
		return rawString;
	}

    /**
     *
     * @return accessor method for the fullstring
     */
    public String getFS() {return fullstring;}

	public void pong()
	{
		send("PONG " + wordParse(rawString, 1) + "\r\n");
	}

	/**
		@param text sting to parse
		@param pos return word at this position
		@return split a string along whitespaces and returns the word at a specific position
	*/
	private String wordParse(String text, int pos)
	{
		String [] words = text.split("\\s");
		return words[pos];
	}

	private void send(String data)
	{
		try 
		{
			System.out.println("Send >> " + data);
			out.write(data);
			out.flush();
		}

		catch(IOException e)
		{
			errorLogger.log(e.toString());
		}
	}

	private String recv()
	{
		String data = "";

		try
		{	
			data = in.readLine();
			return data;
		}
		catch (IOException e)
		{
			errorLogger.log(e.toString());
		}

		return data;
	}
}