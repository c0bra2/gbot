/**
 * Created by Josh on 5/30/2015.
 */
public final class RegCheck {

    /**
     * sends a query to NickVerv to determine the registration status of the nickname
     * @param bot a JHat bot
     * @param uNick String of a irc nickname to be evaluated
     */
    public static void checkRegistered(JHat bot, String uNick){
        bot.put("NS info "+uNick);
    }

    /**
     * sends a query to NickVerv to determine the identified status of the nickname
     * @param bot a JHat bot
     * @param uNick String of a irc nickname to be evaluated
     */
    public static void checkIdentified(JHat bot, String uNick){
        bot.put("NS status "+uNick);
    }

    /**
     *takes NickServ input and returns a status string if applicable
     * @param lineIn string array of an irc text line from NickServ
     * @return a string indicating the status given by NickServ
     */
    public static String takeNickServ(String [] lineIn){
        String nickTextOut = "line_Not_Relevant";

        if (lineIn[0].equals("STATUS")){
            if(isNickIdentified(lineIn)){
                nickTextOut = "identified";
            }
            else{
                nickTextOut = "unidentified";
            }
        }
        else if(lineIn[0].equals("Nick")){
            nickTextOut = "unregistered";
        }
        else if(lineIn.length > 2 && lineIn[2].equals("realname")){
            nickTextOut = "registered";
        }
        else {}

        return nickTextOut;
    }

    /**
     * Takes NickServ response to a Status query and returns it in boolean form
     * @param lineIn String array of message text
     * @return true if identified, false if unidentified
     */
    private static boolean isNickIdentified(String [] lineIn){
        boolean isIdentified = false;
        if (lineIn[0].equals("STATUS")){
            if(lineIn[2].equals("0")){//0 - no such user online or nickname not registered
                isIdentified = false;
            }
            else if(lineIn[2].equals("1")){//1 - user not recognized as nickname's owner
                isIdentified = false;
            }
            else if(lineIn[2].equals("2")){//2 - user recognized as owner via access list only
                isIdentified = false;
            }
            else if(lineIn[2].equals("3")){//3 - user recognized as owner via password identification
                isIdentified = true;
            }
            else{

            }
        }
        return isIdentified;
    }
}

