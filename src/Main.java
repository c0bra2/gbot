import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.SplittableRandom;

public class Main {
	public static void main(String args[]){
		//allocate memory by declaring variables
		UserLastSeen lastSeen;
		JHat bot;
        Vote voteObj;
		AdminTools tools;
		AdminUsers admin;
		SpamDetection detect;
		Scanner scanner = new Scanner(System.in);
		RandomName ranGen;
		ChatLogging chatlog;
        AwayPM awayPM;
        Question question;
        Math math;
		String [] line;
        String talker;
		Boolean flag = true;
		String repeatLine = "";
		String ircServer = "";
		String channel = "";
        String botNick = "n";
		String authorizedUser = "";
		int port;

		//create instances of the objects and assign to variables
		detect = new SpamDetection();
		admin = new AdminUsers();
        voteObj = new Vote();
		ranGen = new RandomName("bot");
		lastSeen = new UserLastSeen();
		chatlog = new ChatLogging();
        awayPM = new AwayPM();
        question = new Question();
        math = new Math();
		tools = new AdminTools();

		System.out.print("IRC SERVER: ");
		ircServer = scanner.nextLine();
		System.out.print("Channel: ");
		channel = scanner.nextLine();
        System.out.print("Bot Nickname (leave blank for random name): ");
        botNick = scanner.nextLine();
        if(botNick.isEmpty()){botNick = ranGen.getName();}
		System.out.print("Bot Admin: ");
		authorizedUser = scanner.nextLine();
		System.out.print("Port: ");
		port = scanner.nextInt();

		bot = new JHat(botNick, ircServer, channel, port);

		//add botowners
		admin.addOwner(authorizedUser);

		//main logic of gbot
		while (true){
			bot.parse();
			line = bot.getLine();
            talker = bot.getTalker();
			System.out.println(bot.getRS());

			if (!flag){
				detect.checkForSpam(bot);
				chatlog.log(bot.getTalker(), line); //log channel activity
			}
			//This block of code is run once and is used
			//to join the channel when the bot starts up
			if (bot.getRS().contains("This is an autmated message to find")){
				if (flag)
					bot.joinChan(channel);
				flag = false;
			}

			//Check to see if a person is joining the channel
			if (bot.getRS().contains("JOIN")) {
				//make sure to register when this user was seen entering the channel
				lastSeen.seenUser(bot.getTalker());

		}

			//area where commands that anyone can run go:

			//checks to see when a user was last seen
            if (line[0].equals("!vote") && line.length > 1){
                voteObj.castVote(line[1], bot);
            }
            if (line[0].equals("!vote") && line.length == 1){
                voteObj.castVote(bot);
            }
			if (line[0].equals("!seen") && line.length > 1){
				bot.write(lastSeen.when(line[1]));
			}

			//repeats something a user said in IRC
			if (line[0].equals("!say") && line.length > 1){
                bot.write(bot.getFS());
			}

            //leave a message for a user that is not online
			if (line[0].equals("!awayPM") && line.length > 1){
				String fullText="";
                String fromUser = bot.getTalker();
                String toUser = line[1];
                //String testUser = ("NS info "+toUser);
                String returnMsg ="";
				for(int i=2; i < line.length; i++){
					fullText = fullText.concat(line[i] +" ");
				}
                //check if toUser is registered
                //bot.put(testUser);
                bot.write(bot.getTalker(), awayPM.sendPM(bot.getTalker(), toUser, fullText));
                RegCheck.checkIdentified(bot, fromUser);
                RegCheck.checkRegistered(bot, toUser);


			}

            if (line[0].equals("!checkPM")){
                awayPM.addPendingCheck(bot.getTalker());
                RegCheck.checkIdentified(bot, bot.getTalker());
                bot.write(bot.getTalker(), "Validating...");
            }

			//user validation for awayPM
            if(talker.equals("NickServ")){
                awayPM.validation(bot, line);
            }

            //return a yahoo answers result
            if (line[0].equals("!question") && line.length > 1){
                String fullText="";
                for(int i=1; i < line.length; i++){
                    fullText = fullText.concat(line[i] +" ");
                }
                bot.write(question.getYahooQuery(fullText));
            }

            //return a dictionary.com result
            if (line[0].equals("!define") && line.length > 1){
                String fullText="";
                for(int i=1; i < line.length; i++){
                    fullText = line[i] +" ";
                }
                bot.write(question.getDictionaryQuery(fullText));
            }

            //return a yahoo answers result
            if (line[0].equals("!calc") && line.length > 1){
                String fullText="";
                for(int i=1; i < line.length; i++) {
                    fullText = fullText.concat(line[i] +" ");
                }
                bot.write(math.onMessage(fullText));
            }

            if (admin.isOwner(bot.getTalker())){
				//area where only botOwner (authorized users) commands can go:

				//mutes one or more users
				if (line[0].equals("!mute")){
					tools.mute(bot, line);
                }

                //kickbans one or more users
				if (line[0].equals("!kickban")){
					tools.kickban(bot, line);
				}

				//gives voice to one or more users
				if (line[0].equals("!voice")){
					tools.giveVoice(bot, line);
				}

				//Sends a list of the bot admin to the channel
				if (line[0].equals("!getOwners")){
					bot.write(admin.getOwners());
					System.out.println(admin.getOwners());
				}
				//adds a new user to admin list and elevates privledges
				if (line[0].equals("!addOwner") && line.length > 1){
					admin.addOwner(line[1]);
					bot.write(line[1] + " was added to bot owners list!");
                    bot.put("MODE " + channel + " +q " + line[1]);
					bot.put("MODE " + channel + " +a " + line[1]);
					bot.put("MODE  " + channel + " +o " + line[1]);
                    bot.put("MODE " + channel + "+h " + line[1]);
				}
				//removes a user from being an admin
				if (line[0].equals("!removeOwner") && line.length > 1){
					admin.removeOwner(line[1]);
					bot.write(line[1] + " was removed from bot owners list!");
					bot.write(line[1] + " was added to bot owners list!");
					bot.put("MODE " + channel + " -q " + line[1]);
					bot.put("MODE " + channel + " -a " + line[1]);
					bot.put("MODE  " + channel + " -o " + line[1]);
                    bot.put("MODE " + channel + "-h " + line[1]);
				}
				//causes the bot to leave irc and close the program
				if (line[0].equals("!quit")){
					bot.write("cya later " + bot.getTalker() + " :D");
					lastSeen.saveMap();
                    awayPM.saveMap();
                    System.exit(0);
				}
				//causes the bot to join a new channel
				if (line[0].equals("!join") && line.length > 1){
					bot.joinChan(line[1]);
				}
			}
		}
	}
}