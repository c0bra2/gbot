/**
 * Created by Josh on 5/4/2015.
 *
 * notes: answeres.yahoo.com answers in class="answer-detail Fw-n"
 * question in class="Mstart-75 Mr-14 Pos-r"
 * https://answers.yahoo.com/search/search_result?fr=uh3_answers_vert_gs&type=2button&p=test%20this%20page
 *
 * to do: clean up results
 */

import java.net.*;
import java.io.*;

public class Question {

    Define define;
    Yahoo yahoo;

    public Question (){
        //rawHTML = "";
        //answer = "test";
        // do the things here
        define = new Define();
        yahoo = new Yahoo();
    }

    public class Define{

        /**
         Primary define method, takes string of a question, queries dictionary.com and returns definition
         @param question a string of the word
         @return definition string
         */
        public String dictionaryQuery(String question){

            return parseAnswerHTML(pullHTML(inputToURL(question,
                    "http://dictionary.reference.com/browse/").concat("?s=t")));

        }

        public String parseAnswerHTML(String rawHTML){
            if(rawHTML == "Invalid"){
                return "Question failed: connection issue";
            }
            else if(rawHTML.contains("area-noresults")|| rawHTML == "not_found"){
                return "Hmmm, I don't think that's a word.";
            }
            else if(rawHTML.contains("Did you mean ")){
                String suggest ="Did you mean: ";
                int startIndex = rawHTML.indexOf(">",rawHTML.indexOf("data-syllable", rawHTML.indexOf("Did you mean")))+1;
                int endIndex = rawHTML.indexOf("</span>", startIndex);
                suggest = suggest.concat(rawHTML.substring(startIndex, endIndex));
                suggest = suggest.concat("?");
                return suggest;
            }
            else if(rawHTML == "unknown"){
                return "An unknown error occurred in the !Question class";
            }
            else {


                String answer = "";
                int startIndex = rawHTML.indexOf("def-content", 250) + 13;
                int endIndex = rawHTML.indexOf("</div>", startIndex);
                answer = rawHTML.substring(startIndex, endIndex);
                if(answer.contains("<div")) {
                    answer = answer.substring(0, answer.indexOf("<div"));
                }
                answer = answer.replace("&#039;", "'");
                answer = answer.replace("<br>", "");
                answer = answer.replace("&quot;","\"");
                answer = answer.replace("&gt;",">");
                answer = answer.replace("&lt;", "<");
                answer = answer.replace("&nbsp;"," ");
                answer = answer.replace("&amp;","&");
                answer = answer.replace("\n","");

                //format answer
                answer = answer.substring(1);

                return answer;
            }
        }
    }


    public class Yahoo{
        /**
         Primary question method, takes string of a question, queries yahoo and returns answer
         @param question a string of the question
         @return answer string
         */
        public String yahooQuery(String question){
            return parseAnswerHTML(pullHTML(parseResultsHTML(pullHTML(inputToURL(question,
                    "https://answers.yahoo.com/search/search_result?fr=uh3_answers_vert_gs&type=2button&p=")))));

        }

        /**
         concatenates the search terms to a yahoo  answer URL
         @param rawHTML The HTML string to be parsed
         @return a string of the  yahoo URL
         */
        public String parseResultsHTML(String rawHTML){
            if(rawHTML == "Invalid"){
                return "Invalid";
            }
            if(rawHTML.contains("error-message")){
                return "error-message";
            }
            if(rawHTML == "unknown"){
                return "An unknown error occurred in the !Question class";
            }

            String urlStringA = "";
            String partialURL = "https://answers.yahoo.com";
            int qTag =rawHTML.indexOf("question-title\">", 61000);
            int startIndex =rawHTML.indexOf("href=\"",qTag) + 6;
            int endIndex =rawHTML.indexOf("\">",qTag + 16);
            urlStringA = partialURL.concat(rawHTML.substring(startIndex,endIndex));

            return urlStringA;
        }

        /**
         outputs the final question string (subject to change)
         @param rawHTML The HTML string to be parsed
         @return formatted answer string
         */
        public String parseAnswerHTML(String rawHTML){
            if(rawHTML == "Invalid" || rawHTML == "not_found"){
                return "Question failed: connection issue";
            }
            if(rawHTML == "error-message"){
                return "I don't know, google it.";
            }
            if(rawHTML == "unknown"){
                return "An unknown error occurred in the !Question class";
            }
            String question = "";
            int startInd = rawHTML.indexOf("<title>") + 7;
            int endInd = rawHTML.indexOf("| Yahoo Answers</title>", startInd + 7);
            question = rawHTML.substring(startInd, endInd);
            question = question.replace("&#039;", "'");
            question = question.replace("<br>","");
            question = question.replace("&quot;","\"");
            question = question.replace("&gt;",">");
            question = question.replace("&lt;","<");
            question = question.replace("&nbsp;"," ");
            question = question.replace("&amp;","&");
            question = question.replace("\n","");

            String answer ="";

            if(rawHTML.contains("Best Answer:</span>")){
                int startIndex = rawHTML.indexOf("<span class=\"ya-q-full-text\">", 11000) + 29;
                int endIndex = rawHTML.indexOf("</span>", startIndex );
                answer = rawHTML.substring(startIndex, endIndex);
            }
            else {
                int startIndex = rawHTML.indexOf("answer-detail Fw-n", 61000) + 63;
                int endIndex = rawHTML.indexOf("</span>", startIndex + 40);
                answer = rawHTML.substring(startIndex, endIndex);
            }
            answer = answer.replace("&#039;", "'");
            answer = answer.replace("<br>", "");
            answer = answer.replace("&quot;","\"");
            answer = answer.replace("&gt;",">");
            answer = answer.replace("&lt;","<");
            answer = answer.replace("&nbsp;"," ");
            answer = answer.replace("&amp;","&");
            answer = answer.replace("\n","");

            //format answer
            String fullResponse = "Closest match: " + question + " Best answer: " + answer;

            return fullResponse;
        }

    }

    /**
     *
     * @param defWord
     * @return
     */
    public String getDictionaryQuery (String defWord){
        return define.dictionaryQuery(defWord);
    }

    public String getYahooQuery(String inQuestion){
        return yahoo.yahooQuery(inQuestion);
    }

    /**
     populate rawHTML string with raw HTML from webpage
     @param inURL a string of the URL to be pulled
     @return true if rawHTML was successfully populated
     */
    public static String pullHTML(String inURL){
        if(inURL == "Invalid"){
            return "Invalid";
        }
        if(inURL =="error-message"){
            return "error-message";
        }
        String rawHTML = "";
        try{
            URL url = new URL(inURL);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(url.openStream()));

            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                rawHTML = rawHTML.concat(inputLine);
                rawHTML = rawHTML.concat("\n");
            }
            in.close();
        }
        catch (FileNotFoundException e){
            return "not_found";
        }
        catch(UnknownHostException e){
            return "Invalid";
        }
        catch(Exception e){
            e.printStackTrace();
            System.out.println("pullHTML exception");
            return "unknown";
        }
        return rawHTML;
    }

    /**
     Formats a string into a answers.yahoo search
     @param aQuestion unformatted string (question)
     @return string of an answers.yahoo search
     */
    public String inputToURL(String aQuestion, String partialURL){
        String [] questionAry;
        questionAry = aQuestion.split(" ");
        for (int i = 0; i < questionAry.length; i++){
            partialURL = partialURL.concat(questionAry[i]);
            if(i+1 < questionAry.length){
                partialURL = partialURL.concat("%20");
            }
        }

        return partialURL;
    }

}