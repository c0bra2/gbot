/**
 * Created by j on 5/27/2015.
 */
import org.nfunk.jep.*;//math API

public class Math {

    // This JEP object will be used for the calculations.
    private JEP jep;

    public Math() {
        //setName(name);

        // Set up the JEP object's capabilities.
        jep = new JEP( );
        jep.addStandardConstants( );
        jep.addStandardFunctions( );
        jep.setAllowUndeclared(false);
        jep.setImplicitMul(true);
        jep.addComplex( );
    }

    public String onMessage(String expression) {

        expression = expression.trim( ).toLowerCase( );

        // Default answer.
        String answer = "Unable to parse your input.";

        try {
            jep.parseExpression(expression);
            if (!jep.hasError( )) {
                String real = String.valueOf(jep.getValue( ));
                String complex = String.valueOf(jep.getComplexValue( ));
                answer = real;

                // Remove the decimal point if the number is integral.
                if (real.endsWith(".0")) {
                    answer = real.substring(0, real.length( ) - 2);
                }

                if (!complex.endsWith(" 0.0)")) {
                    answer = "Complex " + complex;
                }
            }
            return answer;
        }
        catch (Exception e) {
            return "Input failed to parse.";
        }
    }

    /*public void onMessage(String channel, String sender,
                          String login, String hostname, String message) {

        message = message.trim( ).toLowerCase( );

        // Check for the "calc" command.
        if (message.startsWith("calc ")) {
            String expression = message.substring(5);

            // Default answer.
            String answer = "Unable to parse your input.";

            try {
                jep.parseExpression(expression);
                if (!jep.hasError( )) {
                    String real = String.valueOf(jep.getValue( ));
                    String complex = String.valueOf(jep.getComplexValue( ));
                    answer = real;

                    // Remove the decimal point if the number is integral.
                    if (real.endsWith(".0")) {
                        answer = real.substring(0, real.length( ) - 2);
                    }

                    if (!complex.endsWith(" 0.0)")) {
                        answer = "Complex " + complex;
                    }
                }
            }
            catch (Exception e) {
                // Do nothing.
            }

            sendMessage(channel, sender + ": " + answer);
        }

    }*/

}