public class AdminTools {
    public void kickban(JHat bot, String [] users){
        //perform action for every user mentioned
        for (int i = 1; i < users.length; i++){
            bot.put("MODE " + bot.getChan() + " +b " + users[i] + "\r\n");
            bot.put("KICK " + bot.getChan() + " " + users[i] + "\r\n");
        }
    }
    public void mute(JHat bot, String [] users){
        //perform action for every user mentioned
        for (int i = 1; i < users.length; i++){
            bot.put("MODE " + bot.getChan() + " -v " + users[i] + "\r\n");
        }
    }

    public void giveVoice(JHat bot, String[] users){
        //perform action for every user mentioned
        for (int i = 1; i < users.length; i++){
            bot.put("MODE " + bot.getChan() + " +v " + users[i] + "\r\n");
        }
    }
}
