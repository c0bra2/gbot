import java.util.*;
import java.util.Date;

public class SpamDetection {
    private TreeMap<String, Counter> tmap;
    private Date priorDate;

    public SpamDetection(){
        priorDate = new Date();
        tmap = new TreeMap();
    }

    public void checkForSpam(JHat bot)
    {
       if (tmap.containsKey(bot.getTalker())){
           tmap.get(bot.getTalker()).count++;
       }
       else {
         tmap.put(bot.getTalker(), new Counter());
       }

       if (timeElapsed(priorDate) > 4){
           if (tmap.get(bot.getTalker()).count >= 4){
               if (!(bot.getTalker() == bot.getBotNick()))
               bot.put("KICK " + bot.getChan() + " " + bot.getTalker() + " Typing messages too fast, slow down!");
           }
           tmap.get(bot.getTalker()).count = 0;
           priorDate = new Date();
       }
    }

    private long timeElapsed(Date begin) {
        String res = "";
        Date currDateTime = new Date();

        Date startDate = begin;
        long different = currDateTime.getTime() - startDate.getTime();

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        return elapsedSeconds;
    }

    public class Counter{
        public int count = 0;
    }
}
