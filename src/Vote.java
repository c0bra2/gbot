import java.util.Date;

public class Vote {
    private Date voteStarted;
    private boolean voting = false;
    private int votes = 0;
    private String votedUser = "";
    private String voters = ""; //keep track of people who voted

    public void castVote(JHat bot){
        if (voting){
            //check to see if voting session expired (1 min) and a previous user didn't already vote
            if (!(timeElapsed(voteStarted) >= 1) && !(voters.contains(bot.getTalker()))){
                votes++;
                voters += bot.getTalker() + " ";
                if (votes > bot.countUsers()/2){
                    //voting successfull
                    if(!(votedUser == bot.getBotNick()))
                        bot.put("KICK " + bot.getChan() + " "  + votedUser);
                }
                else{
                    bot.write(votes + " vote(s) cast");
                }
            }
            else if (timeElapsed(voteStarted) >= 1){
                //voting has expired
                bot.write("Voting session has expired!");
                voting = false;
            }
        }
        else{
            bot.write("There has not been a vote initiated, please try !vote Username");
        }
    }

    public void castVote(String userName, JHat bot){
        if (!voting){
            //refresh information
            voteStarted = new Date(); //create object with current date
            voting = true;
            votes = 1;
            votedUser = userName;
            voters += bot.getTalker() + " ";
            bot.write("A vote to kick " + votedUser + " has been initiated. 1 vote(s) cast");
        }
        else{
            bot.write("There has already been a vote initiated, please try !vote by itself to cast your vote");
        }

    }

    /**
     * @param begin a previously generated date object
     * @return time elapsed since date
     */
    public long timeElapsed(Date begin) {
        String res = "";
        Date currDateTime = new Date();

        Date startDate = begin;
        long different = currDateTime.getTime() - startDate.getTime();

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;

        return elapsedMinutes;
    }
}
