/**
 * Created by Josh on 5/21/2015.
 *
 * notes:
 * command intakes message
 * recipient registration validation
 * store message
 *
 * user logon listener
 * registered usermame validation
 * search for user(has messages)
 * return message/remove message
 * check for more messages
 *
 * todo:
 * send/receive register check
 * remove old messages
 * test multi message handling
 */

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;


public class AwayPM {
    private TreeMap tMap;
    private List<String> pendingTo;
    private List<String> pendingFrom;
    private List<String> pendingCheck;
    private List<String> senderIdentified;

    /**
     Constructor method for AwayPM
     This method determines whether or not a serialized object of TreeMap has
     already been saved in the current directory. If a serialized object exists
     it is read into memory, Otherwise it is created
     */
    public AwayPM(){
        //check to see if serialized object already exist in the current directory
        pendingTo  = new ArrayList<String>();
        pendingFrom = new ArrayList<String>();
        pendingCheck = new ArrayList<String>();
        senderIdentified = new ArrayList<String>();

        File f = new File("awayObj.bin");
        if(f.exists() && !f.isDirectory()) {
            //file exist and now we must read from the file
            try{
                ObjectInputStream savedSer = null;
                savedSer = new ObjectInputStream(new BufferedInputStream(new FileInputStream("awayObj.bin")));
                tMap = (TreeMap) savedSer.readObject();
            }
            catch (ClassNotFoundException e){
                e.printStackTrace();
            }
            catch (IOException e ){
                e.printStackTrace();
            }
        }
        else{
            //file does not exist, so we must serialize and store the object
            try {
                tMap = new TreeMap();
                ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("awayObj.bin"));
                out.writeObject(tMap);
                out.close();
            }
            catch (IOException e ){
                e.printStackTrace();
            }
        }
    }

    /**
     This method stores a PM into the binary tree
     @param sendUser User sending the PM
     @param receiveUser User to receive the PM
     @param PM the private message to be sent (stored)
     */
    public String sendPM(String sendUser, String receiveUser, String PM){
        List<String> userPMs;
        String fullMessage ="";
        Date sentOn = new Date();
        fullMessage = fullMessage.concat(sendUser + " on " + sentOn + ": " + PM);

        //check if the user already has messages
        if(tMap.containsKey(receiveUser)){
            userPMs = (List)tMap.get(receiveUser);
        }
        else {
            userPMs = new ArrayList<String>();
        }

        userPMs.add(fullMessage);
        tMap.put(receiveUser, userPMs); //save the PM in the binary tree object
        pendingTo.add(receiveUser);
        pendingFrom.add(sendUser);


        return "Message pending";
    }

    /**
     * returns all messages for a specified user.
     * @param aUser User to query about when they were last seen
     * @return userPMs List object of all PMs saved for the given user
     */
    public List<String> getMessages(String aUser){
        List<String> userPMs;

        if (tMap.containsKey(aUser)){
           userPMs = (List)tMap.get(aUser);
            tMap.remove(aUser);
        }
        else{
            String none="You have no saved messages on this channel.";
            userPMs = new ArrayList<String>();
            userPMs.add(none);

        }
        if(pendingCheck.contains(aUser)) {
            pendingCheck.remove(pendingCheck.indexOf(aUser));
        }
        return userPMs;
    }

    /**
     * check if there is a pending message to be saved to a user
     * @param nick string of the user in question
     * @return boolean indicating if there is a pending PM to be saved for the user
     */
    private boolean checkPendingTo(String nick){
        boolean isPend = false;
        if (pendingTo.contains(nick)) {
            isPend = true;
        }
        return isPend;
    }

    /**
     * check if a user is trying to save a message
     * @param nick string of the user in question
     * @return boolean indicating if user is trying to save a message
     */
    private boolean checkPendingFrom(String nick){
        boolean isPend = false;
        if (pendingFrom.contains(nick)) {
            isPend = true;
        }
        return isPend;
    }

    /**
     * takes a user with a pending message to be saved for them
     * and returns the user trying to save it
     * @param toUser string of user with a PM pending to be saved for them
     * @return string of user trying to save the PM
     */
    private String getSenderFromReceiver(String toUser){
        return pendingFrom.get(pendingTo.indexOf(toUser));
    }

    /**
     * handles the implications if a user passed or failed a registration check
     * @param receiveUser string of the user that was checked
     * @param passed boolean indicating if the user passed registration check
     */
    private void regChecked(String receiveUser, boolean passed){
        if(passed == false){
            if (tMap.containsKey(receiveUser)) {
                tMap.remove(receiveUser);
            }
        }
        int index = pendingTo.indexOf(receiveUser);
        if(senderIdentified.contains(pendingFrom.get(index))) {
            senderIdentified.remove(senderIdentified.indexOf(pendingFrom.get(index)));
        }
        //String sender = pendingFrom.get(index);
        pendingTo.remove(index);
        pendingFrom.remove(index);

        //return sender;
    }

    /**
     * tells the class that this user is waiting to check PMs
     * @param toUser string of the user in question
     */
    public void addPendingCheck(String toUser) {
        pendingCheck.add(toUser);
    }

    /**
     * checks to see if the user is awaiting a message check
     * @param toUser string of the user in question
     * @return boolean indicating if the user is awaiting a message check
     */
    private boolean isUserPendingCheck(String toUser){
        boolean isPending = pendingCheck.contains(toUser);
        if(isPending){
            pendingCheck.remove(pendingCheck.indexOf(toUser));
        }
        return isPending;
    }

    /**
     * handles authentication based on the feedback from NickServ
     * @param bot A JHat for the current bot
     * @param line the string array for the output line from the JHat
     */
    public void validation(JHat bot, String [] line){
        String status = RegCheck.takeNickServ(line);

        if(status.equals("line_Not_Relevant")){}
        else if(status.equals("unregistered")){
            String toSterilized = line[1];
            toSterilized = toSterilized.substring(1, toSterilized.length() - 1);
            if(checkPendingTo(toSterilized)) {//for awayPM
                bot.write(getSenderFromReceiver(toSterilized), "Message failed: " + toSterilized + " is not a registered nickname");
                regChecked(toSterilized, false);
            }
        }
        else if (status.equals("registered")){
            if(checkPendingTo(line[0])) {//for awayPM
                String sender = getSenderFromReceiver(line[0]);
                if(senderIdentified.contains(sender)) {
                    regChecked(line[0], true);
                    bot.write(sender, "Message to " + line[0] + ": saved successfully");
                }
                else{
                    regChecked(line[0], false);
                }
            }
        }
        else if (status.equals("identified")){// checked user is in line[1]
            if(isUserPendingCheck(line[1])) {//for awayPM
                String receiver = line[1];
                List<String> allPMs = getMessages(receiver);
                for (int i = 0; i < allPMs.size(); i++) {
                    bot.write(line[1], allPMs.get(i));
                }
            }
            if(checkPendingFrom(line[1])) {//for awayPM
                senderIdentified.add(line[1]);
                bot.write(line[1], "Sender Validated");
            }
        }
        else {//unidentified, checked user is in line[1]
            if(isUserPendingCheck(line[1])) {
                //removePendingCheck(line[1]);
                if(pendingCheck.contains(line[1])) {
                    pendingCheck.remove(pendingCheck.indexOf(line[1]));
                }
                bot.write(line[1], "Validation failed: You must be identified to use PMs");
            }
            if(checkPendingFrom(line[1])) {//for awayPM
                bot.write(line[1], "Validation failed: You must be identified to Send PMs");
            }
        }
    }

    /**
     * This method is used to save the TreeMap object in memory to the harddrive so that it may be persistently used the
     * next time the bot is executed
     */
    public void saveMap(){
        try{
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("awayObj.bin"));
            out.writeObject(tMap);
            out.close();
        }
        catch (IOException e ){
            e.printStackTrace();
        }
    }


}
